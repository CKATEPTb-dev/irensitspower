package ru.ckateptb.irens.api;

public interface ICommand {
    String getName();

    Executor getExecutor();

    interface Executor {
        boolean execute(String... args);
    }
}
