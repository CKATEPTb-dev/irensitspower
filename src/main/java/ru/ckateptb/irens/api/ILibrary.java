package ru.ckateptb.irens.api;

import ru.ckateptb.irens.IrensItsPower;

import java.util.List;

import static ru.ckateptb.irens.storage.SQLQuery.ADD_CONTENT_TO_TABLE;
import static ru.ckateptb.irens.storage.SQLQuery.REMOVE_CONTENT_FROM_TABLE;

public interface ILibrary /*<T extends ILibraryContent> prohibit the use of content from another library*/ {
    default boolean add(ILibraryContent content) {
        if (getContent().contains(content)) return false;
        IrensItsPower.getSqLite().execute(String.format(ADD_CONTENT_TO_TABLE, getName()), content.getName(), content.getAuthor());
        return getContent().add(content); //always true
    }

    default boolean remove(ILibraryContent content) {
        if (getContent().removeIf(content::equals)) {
            IrensItsPower.getSqLite().execute(String.format(REMOVE_CONTENT_FROM_TABLE, getName()), content.getName(), content.getAuthor());
            return true;
        }
        return false;
    }

    List<ILibraryContent> getContent();

    String getName();
}
