package ru.ckateptb.irens.api;

public interface ILibraryContent extends Comparable<ILibraryContent> {
    @Override
    default int compareTo(ILibraryContent o) {
        return getName().compareTo(o.getName());
    }

    String getAuthor();

    String getName();
}
