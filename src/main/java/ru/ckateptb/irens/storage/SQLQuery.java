package ru.ckateptb.irens.storage;

public class SQLQuery { //should replace '%s' to '?'?
    public static final String CREATE_TABLE = "create table if not exists %s (`name` text not null, `author` text not null);";
    public static final String ADD_CONTENT_TO_TABLE = "insert into %s (`name`,`author`) values (?,?)";
    public static final String REMOVE_CONTENT_FROM_TABLE = "delete from %s where `name` = ? and `author` = ?";
    public static final String SELECT_ALL_FROM_TABLE = "select * from %s order by `name`";
}
