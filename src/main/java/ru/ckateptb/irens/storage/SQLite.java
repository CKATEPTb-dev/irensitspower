package ru.ckateptb.irens.storage;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLite {
    private final HikariConfig config;
    private final File file;
    protected Connection connection;

    public SQLite(File file) {
        this.file = file;
        this.config = new HikariConfig();
    }

    public void setup(String... initQuery) {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) return;
        this.config.setJdbcUrl("jdbc:sqlite://" + file); //TODO Read from config and add mysql impl with(orm)
        this.config.setDriverClassName("org.sqlite.JDBC");
        connect();
        for (String query : initQuery) {
            execute(query);
        }
    }

    public void connect() {
        try {
            HikariDataSource dataSource = new HikariDataSource(config);
            dataSource.setConnectionTimeout(0); //24d 20h 31m 23s
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            if (!connection.isClosed()) connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void execute(final String query, final Object... args) {
        prepare(ps -> {
            int i = 1;
            for (Object x : args) {
                ps.setObject(i++, x);
            }
            ps.execute();
        }, query);
    }

    public void execute(final QueryListener ql, final String query, Object... args) {
        prepare(ps -> {
            int i = 1;
            for (Object x : args) {
                ps.setObject(i++, x);
            }
            ql.onInteract(ps.executeQuery());
        }, query);
    }

    public void prepare(final ExecuteListener el, final String query) {
        try {
            if (connection.isClosed()) connect();
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            el.onInteract(ps);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface QueryListener {
        void onInteract(ResultSet resSet) throws Exception;
    }

    public interface ExecuteListener {
        void onInteract(PreparedStatement resSet) throws Exception;
    }
}