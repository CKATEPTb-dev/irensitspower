package ru.ckateptb.irens.command;

import ru.ckateptb.irens.api.ICommand;
import ru.ckateptb.irens.library.LibraryFactory;

public class LibrariesCommand implements ICommand {
    @Override
    public String getName() {
        return "libraries";
    }

    @Override
    public Executor getExecutor() {
        return args -> {
            System.out.println(LibraryFactory.getLibraryNames());
            return true;
        };
    }
}
