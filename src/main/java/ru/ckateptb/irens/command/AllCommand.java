package ru.ckateptb.irens.command;

import ru.ckateptb.irens.api.ICommand;
import ru.ckateptb.irens.api.ILibrary;
import ru.ckateptb.irens.library.LibraryFactory;
import ru.ckateptb.irens.utils.Validate;

public class AllCommand implements ICommand {
    @Override
    public String getName() {
        return "all";
    }

    @Override
    public Executor getExecutor() {
        return args -> {
            if (Validate.isTrue(args.length >= 1, "Not enough parameters, use \"help\" to display help")) {
                ILibrary library = LibraryFactory.getLibrary(args[0]);
                if (Validate.isTrue(library != null, "Unknown library, type \"libraries\" to display list of libraries")) {
                    System.out.println(library.getContent());
                }
            }
            return true;
        };
    }
}
