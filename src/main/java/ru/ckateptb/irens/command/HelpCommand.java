package ru.ckateptb.irens.command;

import ru.ckateptb.irens.api.ICommand;

public class HelpCommand implements ICommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public Executor getExecutor() {
        return args -> {
            System.out.println("add {library} - add content to library");
            System.out.println("all {library} - display all content in library");
            System.out.println("remove {library} - remove content from library");
            System.out.println("libraries - display list of libraries");
            System.out.println("help - display this information again");
            System.out.println("exit - exit (lol)");
            return true;
        };
    }
}
