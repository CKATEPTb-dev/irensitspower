package ru.ckateptb.irens.command;

import org.jline.reader.LineReader;
import ru.ckateptb.irens.IrensItsPower;
import ru.ckateptb.irens.api.ICommand;
import ru.ckateptb.irens.api.ILibrary;
import ru.ckateptb.irens.api.ILibraryContent;
import ru.ckateptb.irens.library.LibraryFactory;
import ru.ckateptb.irens.library.content.DefaultLibraryContent;
import ru.ckateptb.irens.utils.Validate;

public class AddCommand implements ICommand {
    @Override
    public String getName() {
        return "add";
    }

    @Override
    public Executor getExecutor() {
        return args -> {
            if (Validate.isTrue(args.length >= 1, "Not enough parameters, use \"help\" to display help")) {
                ILibrary library = LibraryFactory.getLibrary(args[0]);
                if (Validate.isTrue(library != null, "Unknown library, type \"libraries\" to display list of libraries")) {
                    System.out.println("Enter content author...");
                    LineReader reader = IrensItsPower.getInstance().getConsole().getReader();
                    String author = Validate.adapt(reader.readLine("> author: "));
                    System.out.println("Enter content name...");
                    String name = Validate.adapt(reader.readLine("> name: "));
                    if (author.length() < 2 || name.length() < 2) return true;
                    ILibraryContent content = new DefaultLibraryContent(name, author);
                    for (ILibraryContent libraryContent : library.getContent()) {
                        if (!Validate.isTrue(!content.equals(libraryContent),
                                "Library %s already contains %s", library.getName(), content)) {
                            return true;
                        }
                    }
                    library.add(content);
                    System.out.println(String.format("%s was successfully added to the library %s",
                            content, library.getName()));
                }
            }
            return true;
        };
    }
}
