package ru.ckateptb.irens.command;

import ru.ckateptb.irens.api.ICommand;

import java.util.HashMap;
import java.util.Map;

public abstract class CommandFactory {
    private static final Map<String, ICommand> commandMap = new HashMap<>();

    public static boolean parseAndExecuteIfExists(String line) {
        String name = line.split(" ")[0].toLowerCase();
        String args = line.replaceAll(name, "").trim();
        ICommand command = CommandFactory.commandMap.get(name);
        if (command == null) return false;
        return command.getExecutor().execute(args.split(" "));
    }

    public static void register(ICommand command) {
        CommandFactory.commandMap.put(command.getName(), command);
    }
}
