package ru.ckateptb.irens.console;

import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;

import static org.jline.reader.LineReader.Option.*;

public class JLineConsole {
    private final Terminal terminal;
    private final LineReader reader;

    public JLineConsole() throws IOException {
        this.terminal = TerminalBuilder.builder()
                .jansi(true)
//                .streams(System.in, System.out)
                .dumb(true)
                .build();
        this.reader = LineReaderBuilder.builder()
                .terminal(terminal)
                .option(BRACKETED_PASTE, false)
                .build();
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public LineReader getReader() {
        return reader;
    }
}
