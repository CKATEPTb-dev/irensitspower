package ru.ckateptb.irens.library;

import ru.ckateptb.irens.api.ILibrary;
import ru.ckateptb.irens.api.ILibraryContent;
import ru.ckateptb.irens.library.content.DefaultLibraryContent;
import ru.ckateptb.irens.storage.SQLite;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static ru.ckateptb.irens.storage.SQLQuery.SELECT_ALL_FROM_TABLE;


public class LibraryFactory {
    private static final Map<String, ILibrary> libraryMap = new HashMap<>();

    public static void register(ILibrary library) {
        LibraryFactory.libraryMap.put(library.getName(), library);
    }

    public static void loadLibraryContent(SQLite sqLite) {
        libraryMap.values().forEach(library -> sqLite.execute(res -> {
            while (res.next()) {
                ILibraryContent content = new DefaultLibraryContent(res.getString("name"), res.getString("author"));
                library.getContent().add(content);
            }
        }, String.format(SELECT_ALL_FROM_TABLE, library.getName())));
    }

    public static Set<String> getLibraryNames() {
        return libraryMap.keySet();
    }

    public static ILibrary getLibrary(String name) {
        return libraryMap.get(name);
    }
}
