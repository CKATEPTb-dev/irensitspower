package ru.ckateptb.irens.library.content;

import ru.ckateptb.irens.api.ILibraryContent;

public class DefaultLibraryContent implements ILibraryContent {
    private final String name;
    private final String author;

    public DefaultLibraryContent(String name, String author) {
        this.name = name;
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof DefaultLibraryContent) {
            return this.name.equals(((DefaultLibraryContent) object).name)
                    && this.author.equals(((DefaultLibraryContent) object).author);
        }
        return super.equals(object);
    }

    @Override
    public String toString() {
        return String.format("%s \"%s\"", author, name);
    }
}
