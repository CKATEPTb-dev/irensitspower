package ru.ckateptb.irens.library;

import ru.ckateptb.irens.api.ILibrary;
import ru.ckateptb.irens.api.ILibraryContent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameLibrary implements ILibrary {
    private final List<ILibraryContent> contentList = new ArrayList<>();

    @Override
    public List<ILibraryContent> getContent() {
        Collections.sort(contentList);
        return contentList;
    }

    @Override
    public String getName() {
        return "game";
    }
}
