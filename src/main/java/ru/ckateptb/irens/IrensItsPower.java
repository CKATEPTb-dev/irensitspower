package ru.ckateptb.irens;

import org.jline.reader.LineReader;
import ru.ckateptb.irens.command.*;
import ru.ckateptb.irens.console.JLineConsole;
import ru.ckateptb.irens.library.*;
import ru.ckateptb.irens.storage.SQLite;
import ru.ckateptb.irens.utils.Validate;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.ckateptb.irens.storage.SQLQuery.*;

public class IrensItsPower {
    private static IrensItsPower instance;
    private static SQLite sqLite;

    public static void main(String[] args) throws IOException {
        File file = new File(
                new File(
                        IrensItsPower.class.getProtectionDomain().getCodeSource().getLocation().getPath()
                ).getParentFile(), "irens.db"
        );
        sqLite = new SQLite(file);
        instance = new IrensItsPower();
        instance.registerLibraries();
        List<String> queryList = new ArrayList<>();
        LibraryFactory.getLibraryNames().forEach(name -> {
            queryList.add(String.format(CREATE_TABLE, name));
        });
        sqLite.setup(queryList.toArray(new String[0]));
        LibraryFactory.loadLibraryContent(sqLite);
        instance.registerCommands();
        instance.progress();
    }

    public static IrensItsPower getInstance() {
        return instance;
    }

    public static SQLite getSqLite() {
        return sqLite;
    }

    private final JLineConsole console;

    public IrensItsPower() throws IOException {
        this.console = new JLineConsole();
    }

    public void progress() {
        LineReader reader = this.console.getReader();
        System.out.println("Hi, I am a completed test for working at Irens");
        System.out.println("Type \"help\" to display help");
        while (true) {
            String line = reader.readLine("> ");
            line = Validate.adapt(line);
            if (line.length() == 0) continue;
            if (line.equalsIgnoreCase("exit") || line.equalsIgnoreCase("stop") || line.equalsIgnoreCase("quit")) break;
            if (!CommandFactory.parseAndExecuteIfExists(line)) {
                System.out.println("Unknown command!");
                System.out.println("Type \"help\" to display help");
            }
        }
    }

    public void registerCommands() {// JLine has own mechanics of commands with completer
        CommandFactory.register(new AddCommand());
        CommandFactory.register(new AllCommand());
        CommandFactory.register(new HelpCommand());
        CommandFactory.register(new LibrariesCommand());
        CommandFactory.register(new RemoveCommand());
    }

    public void registerLibraries() {
        LibraryFactory.register(new BookLibrary());
        LibraryFactory.register(new GameLibrary());
        LibraryFactory.register(new MovieLibrary());
        LibraryFactory.register(new SongLibrary());
    }

    public JLineConsole getConsole() {
        return this.console;
    }
}
